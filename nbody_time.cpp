#include <iostream>
#include <vector>
#include <random>
#include <numeric>
#include <cmath>
#include <iomanip>
#include <chrono>


//変数宣言
double G = 10.0; //重力定数
int N = 1000; //粒子の数
double h = 0.01; //タイムステップ幅
//X方向の加速度
double f_vx(int n, const std::vector<double>& m, std::vector<double>& X, std::vector<std::vector<double>>& r){
    int i = n - 3 * N;
    double S = 0.0;
    //重力の計算
    for(int j = 0; j < N; j++){
        if(i == j){
            continue;
        }
        S += m[j] * (X[j] - X[i]) / r[j][i];
    }
    return S * G;
}
//Y方向の加速度
double f_vy(int n, const std::vector<double>& m, std::vector<double>& X, std::vector<std::vector<double>>& r){
    int i = n - 4 * N;
    double S = 0.0;
    //重力の計算
    for(int j = 0; j < N; j++){
        if(i == j){
            continue;
        }
        S += m[j] * (X[j + N] - X[i + N]) / r[j][i];
    }
    return S * G;
}
//Z方向の加速度
double f_vz(int n, const std::vector<double>& m, std::vector<double>& X, std::vector<std::vector<double>>& r){
    int i = n - 5 * N;
    double S = 0.0;
    //重力の計算
    for(int j = 0; j < N; j++){
        if(i == j){
            continue;
        }
        S += m[j] * (X[j + 2 * N] - X[i + 2 * N]) / r[j][i];
    }
    return S * G;
}
//速度or加速度を返す
double f(int n, const std::vector<double>& m, std::vector<double>& X, std::vector<std::vector<double>>& r){
    if (0 <= n && n < 3 *N){
        return X[n + 3 * N];
    } else if (3 * N <= n && n < 4 * N) {
        return f_vx(n, m, X, r);
    } else if (4 * N <= n && n < 5 * N) {
        return f_vy(n, m, X, r);
    } else {
        return f_vz(n, m, X, r);
    }
}
//平均値を引く
void subtract_mean(std::vector<double>& vec){
    double mean = std::accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
    for (auto& elem : vec) {
        elem -= mean;
    }
}
//距離の計算
void caldist(std::vector<std::vector<double>>& r, std::vector<double>& X){
    for(int i = 0; i < N - 1; i++){
        for(int j = i + 1; j < N; j++){
            double r_ij = std::sqrt(0.01 + std::pow(X[j] - X[i], 2) + std::pow(X[j + N] - X[i + N], 2) + std::pow(X[j + 2 * N] - X[i + 2 * N], 2));
            r[i][j] = std::pow(r_ij, 3);
            r[j][i] = r[i][j];
        }
    }
}
//4次ルンゲクッタ
void runge(const std::vector<double>& m, std::vector<double>& X,  std::vector<std::vector<double>>& r){
    std::vector<double> Ka(6 * N), Kb(6 * N), Kc(6 * N), Kd(6 * N), temp(6 * N);
    for(int i = 0; i < 6 * N; i++){
        Ka[i] = h * f(i, m, X, r);
    }
    for(int i = 0; i < 6 * N; i++){
        temp[i] = X[i] + Ka[i] / 2;
    }
    for(int i = 0; i < 6 * N; i++){
        Kb[i] = h * f(i, m, temp, r);
    }
    for(int i = 0; i < 6 * N; i++){
        temp[i] = X[i] + Kb[i] / 2;
    }
    for(int i = 0; i < 6 * N; i++){
        Kc[i] = h * f(i, m, temp, r);
    }
    for(int i = 0; i < 6 * N; i++){
        temp[i] = X[i] + Kc[i];
    }
    for(int i = 0; i < 6 * N; i++){
        Kd[i] = h * f(i, m, temp, r);
    }
    //位置更新
    for(int i = 0; i < 6 * N; i++){
        X[i] = X[i] + (Ka[i] + 2 * Kb[i] + 2 * Kc[i] + Kd[i]) / 6;
    }
}
int main(){
    //ランダムな数字を生成
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dist(1.0, 10.0);
    //位置の初期化
    std::vector<double> x(N), y(N), z(N);
    for(int i = 0; i < N; i++){
        x[i] = dist(gen);
        y[i] = dist(gen);
        z[i] = dist(gen);
    }
    //速度の初期化
    std::vector<double> vx(N), vy(N), vz(N);
    for(int i = 0; i < N; i++){
        vx[i] = dist(gen);
        vy[i] = dist(gen);
        vz[i] = dist(gen);
    }
    subtract_mean(vx);
    subtract_mean(vy);
    subtract_mean(vz);
    //質量の初期化
    std::vector<double> m(N, 20.0/N);
    //ベクターの結合
    std::vector<double> X;
    X.reserve(x.size() + y.size() + z.size() + vx.size() + vy.size() + vz.size());
    X.insert(X.end(), x.begin(), x.end());
    X.insert(X.end(), y.begin(), y.end());
    X.insert(X.end(), z.begin(), z.end());
    X.insert(X.end(), vx.begin(), vx.end());
    X.insert(X.end(), vy.begin(), vy.end());
    X.insert(X.end(), vz.begin(), vz.end());
    //初期化
    int nstep = 1000;
    std::vector<std::vector<double>> r(N, std::vector<double>(N, 0));

    //コア計算
    auto start = std::chrono::high_resolution_clock::now();

    for(int i = 0; i < nstep; i++){
        caldist(r, X);
        runge(m, X, r);
        std::cout << "Step: " << i + 1 << std::endl;
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Total computation time: " << elapsed.count() << " seconds" << std::endl;
    return 0;
}
