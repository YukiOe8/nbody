import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation

# データの読み込み
df = pd.read_csv("positions.csv", header=None, names=["step", "particle", "x", "y", "z"])

# パラメータの設定
nstep = df["step"].max() + 1
N = df["particle"].max() + 1

# フレームごとの位置を格納するリスト
frames = []
for step in range(nstep):
    frame_data = df[df["step"] == step]
    frames.append(frame_data[["x", "y", "z"]].values)

# プロットの設定
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
scat = ax.scatter([], [], [])

def init():
    ax.set_xlim(-3, 3)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    return scat,

def update(frame):
    ax.clear()
    ax.set_xlim(-3, 3)
    ax.set_ylim(-3, 3)
    ax.set_zlim(-3, 3)
    ax.scatter(frame[:, 0], frame[:, 1], frame[:, 2])
    return scat,

ani = FuncAnimation(fig, update, frames=frames, init_func=init, blit=True)

# 動画の保存
ani.save('particle_simulation.mp4', writer='ffmpeg', fps=30)

plt.show()
